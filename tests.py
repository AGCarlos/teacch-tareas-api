from bson import ObjectId
from flask import Flask
from flask_pymongo import PyMongo

from server import _register, _create_user, _update_user, _delete_user, _create_task, _update_task, _delete_task, \
    _create_session, _update_session, _delete_session, _create_log

app = Flask(__name__)
app.config.from_envvar('ENV_FILE_LOCATION')

app.config['MONGO_DBNAME'] = 'task_db'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/task_db'

mongo = PyMongo(app)
task_db = mongo.db


def test_register():
    with app.app_context():
        new_user = {
            "name": "testName",
            "age": 12,
            "image": "testImage",
            "password": "testPassword",
        }
        response = _register(new_user).get_json()
        assert response == {'result': "done"}


def test_create_user():
    with app.app_context():
        # Create new user
        new_user = {
            "name": "testName",
            "age": 12,
            "image": "testImage",
        }
        response = _create_user(new_user).get_json()
        assert response == {'result': "done"}
        # Test if user was created
        find_response = task_db.users.find_one({"name": "testName"})
        assert find_response["age"] == 12
        # Delete user
        assert task_db.users.delete_one({"_id": ObjectId(find_response["_id"])}).acknowledged is True


def test_update_user():
    with app.app_context():
        # Create new user
        new_user = {
            "name": "testName",
            "age": 12,
            "image": "testImage",
        }
        response = _create_user(new_user).get_json()
        assert response == {'result': "done"}
        # Update user and test if it was updated
        new_user_data = {
            "name": "testNameRenamed",
            "age": 12,
            "image": "testImage",
        }
        user = task_db.users.find_one({"name": "testName"})
        response = _update_user(new_user_data, user["_id"]).get_json()
        user_updated = task_db.users.find_one({"name": "testNameRenamed"})
        assert response == {'updated': True}
        assert user_updated["name"] == "testNameRenamed"

        # Delete user
        assert task_db.users.delete_one({"_id": ObjectId(user_updated["_id"])}).acknowledged is True


def test_delete_user():
    with app.app_context():
        # Create new user
        new_user = {
            "name": "testName",
            "age": 12,
            "image": "testImage",
        }
        response = _create_user(new_user).get_json()
        assert response == {'result': "done"}
        user = task_db.users.find_one({"name": "testName"})
        # Delete user
        response = _delete_user(user["_id"]).get_json()
        # Test if user was deleted
        assert response == {'deleted': True}


def test_create_task():
    with app.app_context():
        # Create new task
        new_task = {
            "helpPictos": [],
            "name": "testTask",
            "image": "testImage",
            "createdAt": "testDate",
            "teacherId": "testID"
        }
        response = _create_task(new_task).get_json()
        assert response == {'result': "done"}
        # Test if task was created
        find_response = task_db.tasks.find_one({"name": "testTask"})
        assert find_response["teacherId"] == "testID"
        # Delete task
        assert task_db.tasks.delete_one({"_id": ObjectId(find_response["_id"])}).acknowledged is True


def test_update_task():
    with app.app_context():
        # Create new task
        new_task = {
            "helpPictos": [],
            "name": "testTask",
            "image": "testImage",
            "createdAt": "testDate",
            "teacherId": "testID"
        }
        response = _create_task(new_task).get_json()
        assert response == {'result': "done"}
        task = task_db.tasks.find_one({"name": "testTask"})
        # Update task and test if it was updated
        new_task_data = {
            "helpPictos": [],
            "name": "testTaskUpdated",
            "image": "testImage",
            "createdAt": "testDate",
            "teacherId": "testIDUpdated"
        }
        response = _update_task(new_task_data, task["_id"]).get_json()
        task_updated = task_db.tasks.find_one({"name": "testTaskUpdated"})
        assert response == {'updated': True}
        assert task_updated["teacherId"] == "testIDUpdated"

        # Delete task
        assert task_db.tasks.delete_one({"_id": ObjectId(task_updated["_id"])}).acknowledged is True


def test_delete_task():
    with app.app_context():
        # Create new task
        new_task = {
            "helpPictos": [],
            "name": "testTask",
            "image": "testImage",
            "createdAt": "testDate",
            "teacherId": "testID"
        }
        response = _create_task(new_task).get_json()
        assert response == {'result': "done"}
        task = task_db.tasks.find_one({"name": "testTask"})
        # Delete task
        response = _delete_task(task["_id"]).get_json()
        # Test if task was deleted
        assert response == {'deleted': True}


def test_create_session():
    with app.app_context():
        # Create new session
        new_session = {
            "name": "testSession",
            "image": "testImage",
            "tasks": [
                "testTask1",
                "testTask2",
                "testTask3",
                "testTask4"
            ],
            "animationsDisabled": False,
            "soundsDisabled": False,
            "mode": "sequence",
            "selection": "drag",
            "createdAt": "2020-6-23 19:28:29",
            "teacherId": "testTeacherID",
            "done": True
        }
        _create_session(new_session).get_json()
        # Test if session was created
        find_response = task_db.sessions.find_one({"name": "testSession"})
        assert find_response["teacherId"] == "testTeacherID"
        # Delete session
        assert task_db.sessions.delete_one({"_id": ObjectId(find_response["_id"])}).acknowledged is True


def test_update_session():
    with app.app_context():
        # Create new session
        new_session = {
            "name": "testSession",
            "image": "testImage",
            "tasks": [
                "testTask1",
                "testTask2",
                "testTask3",
                "testTask4"
            ],
            "animationsDisabled": False,
            "soundsDisabled": False,
            "mode": "sequence",
            "selection": "drag",
            "createdAt": "2020-6-23 19:28:29",
            "teacherId": "testTeacherID",
            "done": True
        }
        _create_session(new_session).get_json()
        session = task_db.sessions.find_one({"name": "testSession"})
        # Update session and test if it was updated
        new_session_data = {
            "name": "testSessionUpdated",
            "teacherId": "testTeacherIDUpdated",
            "done": True
        }
        response = _update_session(new_session_data, session["_id"]).get_json()
        session_updated = task_db.sessions.find_one({"name": "testSessionUpdated"})
        assert response == {'updated': True}
        assert session_updated["teacherId"] == "testTeacherIDUpdated"

        # Delete session
        assert task_db.sessions.delete_one({"_id": ObjectId(session_updated["_id"])}).acknowledged is True


def test_delete_session():
    with app.app_context():
        # Create new session
        new_session = {
            "name": "testSession",
            "image": "testImage",
            "tasks": [
                "testTask1",
                "testTask2",
                "testTask3",
                "testTask4"
            ],
            "animationsDisabled": False,
            "soundsDisabled": False,
            "mode": "sequence",
            "selection": "drag",
            "createdAt": "2020-6-23 19:28:29",
            "teacherId": "testTeacherID",
            "done": True
        }
        _create_session(new_session).get_json()
        sessions = task_db.sessions.find_one({"name": "testSession"})
        # Delete session
        response = _delete_session(sessions["_id"]).get_json()
        # Test if session was deleted
        assert response == {'deleted': True}


def test_create_log():
    with app.app_context():
        # Create new log
        new_log = {
            "user_id": "testUserID",
            "session_name": "testLog",
            "time": "0:28",
            "date": "2020-6-23 19:29:49",
            "comment": "testComment",
            "evaluation": 8
        }
        _create_log(new_log).get_json()
        # Test if log was created
        find_response = task_db.logs.find_one({"session_name": "testLog"})
        assert find_response["comment"] == "testComment"
        # Delete log
        assert task_db.logs.delete_one({"_id": ObjectId(find_response["_id"])}).acknowledged is True
