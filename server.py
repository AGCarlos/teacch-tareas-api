from flask import Flask, abort
from flask_cors import CORS
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
from flask_jwt_extended import JWTManager, create_access_token, get_jwt_identity, jwt_required

from bson import json_util, ObjectId
import json
import datetime

from utils.password_utils import hash_password, check_password

app = Flask(__name__)
app.config.from_envvar('ENV_FILE_LOCATION')

CORS(app)
jwt = JWTManager(app)

app.config['MONGO_DBNAME'] = 'task_db'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/task_db'

mongo = PyMongo(app)
task_db = mongo.db


def _register(new_user):
    new_user["password"] = hash_password(new_user)
    task_db.users.insert_one(new_user)

    return jsonify({'result': "done"})


@app.route('/user/register', methods=['POST'])
def register():
    new_user = request.get_json().get('user')
    _register(new_user)


@app.route('/user/login', methods=['POST'])
def login():
    user = request.get_json().get('user')

    hashed = task_db.users.find_one({"email": user['email']})
    if check_password(user['password'], hashed['password']):
        expires = datetime.timedelta(days=7)
        access_token = create_access_token(identity=str(hashed['_id']), expires_delta=expires)
        return jsonify({'token': access_token})
    else:
        abort(401)


@app.route('/user', methods=['GET'])
@jwt_required
def list_users():
    user_id = get_jwt_identity()
    results = task_db.users.find({"teacherId": user_id})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


@app.route('/user_token', methods=['GET'])
@jwt_required
def get_user_with_token():
    user_id = get_jwt_identity()
    results = task_db.users.find_one({"_id": ObjectId(user_id)})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


@app.route('/user/<string:user_id>', methods=['GET'])
@jwt_required
def get_user(user_id):
    results = task_db.users.find_one({"_id": ObjectId(user_id)})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


def _create_user(new_user):
    task_db.users.insert_one(new_user)
    return jsonify({'result': "done"})


@app.route('/user', methods=['POST'])
@jwt_required
def create_user():
    new_user = request.get_json().get('new_user')
    _create_user(new_user)


def _update_user(user_data, user_id):
    results = task_db.users.update_one({"_id": ObjectId(user_id)}, {"$set": user_data})
    return jsonify({'updated': results.raw_result["updatedExisting"]})


@app.route('/user/<string:user_id>', methods=['PUT'])
@jwt_required
def update_user(user_id):
    user_data = request.get_json().get('user_data')
    _update_user(user_data, user_id)


def _delete_user(user_id):
    results = task_db.users.delete_one({"_id": ObjectId(user_id)})
    return jsonify({'deleted': results.acknowledged})


@app.route('/user/<string:user_id>', methods=['DELETE'])
@jwt_required
def delete_user(user_id):
    _delete_user(user_id)


def _create_task(new_task):
    task_db.tasks.insert_one(new_task)
    return jsonify({'result': "done"})


@app.route('/task', methods=['POST'])
@jwt_required
def create_task():
    new_task = request.get_json().get('new_task')
    _create_task(new_task)



@app.route('/task', methods=['GET'])
@jwt_required
def list_task():
    user_id = get_jwt_identity()
    results = task_db.tasks.find({"teacherId": user_id})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


@app.route('/tasks', methods=['PUT'])
@jwt_required
def list_session_tasks():
    obj_ids = list(map(ObjectId, request.get_json().get('ids')))
    print(obj_ids)
    results = task_db.tasks.find({"_id": {"$in": obj_ids}})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


@app.route('/task/<string:task_id>', methods=['GET'])
@jwt_required
def get_task(task_id):
    results = task_db.tasks.find_one({"_id": ObjectId(task_id)})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


def _update_task(task_data, task_id):
    results = task_db.tasks.update_one({"_id": ObjectId(task_id)}, {"$set": task_data})
    return jsonify({'updated': results.raw_result["updatedExisting"]})


@app.route('/task/<string:task_id>', methods=['PUT'])
@jwt_required
def update_task(task_id):
    task_data = request.get_json().get('task_data')
    _update_task(task_data, task_id)


def _delete_task(task_id):
    results = task_db.tasks.delete_one({"_id": ObjectId(task_id)})
    return jsonify({'deleted': results.acknowledged})


@app.route('/task/<string:task_id>', methods=['DELETE'])
@jwt_required
def delete_task(task_id):
    _delete_task(task_id)


def _create_session(new_session):
    result = task_db.sessions.insert_one(new_session)
    return jsonify({'result': str(result)})


@app.route('/session', methods=['POST'])
@jwt_required
def create_session():
    new_session = request.get_json().get('new_session')
    _create_session(new_session)


@app.route('/session', methods=['GET'])
@jwt_required
def list_session():
    user_id = get_jwt_identity()
    results = task_db.sessions.find({"teacherId": user_id})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


def _update_session(session_data, session_id):
    results = task_db.sessions.update_one({"_id": ObjectId(session_id)}, {"$set": session_data})
    return jsonify({'updated': results.raw_result["updatedExisting"]})


@app.route('/session/<string:session_id>', methods=['PUT'])
@jwt_required
def update_session(session_id):
    session_data = request.get_json().get('session_data')
    _update_session(session_data, session_id)


@app.route('/session/<string:session_id>', methods=['GET'])
@jwt_required
def get_session(session_id):
    results = task_db.sessions.find_one({"_id": ObjectId(session_id)})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


def _delete_session(session_id):
    results = task_db.sessions.delete_one({"_id": ObjectId(session_id)})
    return jsonify({'deleted': results.acknowledged})


@app.route('/session/<string:session_id>', methods=['DELETE'])
@jwt_required
def delete_session(session_id):
    _delete_session(session_id)


def _create_log(new_log):
    result = task_db.logs.insert_one(new_log)
    return jsonify({'result': str(result)})


@app.route('/log', methods=['POST'])
@jwt_required
def create_log():
    new_log = request.get_json().get('new_log')
    _create_log(new_log)


@app.route('/log/<string:user_id>', methods=['GET'])
@jwt_required
def list_log(user_id):
    results = task_db.logs.find({"user_id": user_id})
    json_results = json.loads(json_util.dumps(results))

    return jsonify({'result': json_results})


def _delete_logs():
    results = task_db.logs.remove({});
    return jsonify({'deleted': results.acknowledged})


@app.route('/log', methods=['DELETE'])
@jwt_required
def delete_logs():
    _delete_logs()


if __name__ == '__main__':
    app.run(debug=True)
