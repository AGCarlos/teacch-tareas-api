# Teacch-Tareas API

## To run the project
Create a virtual environment for the project, then install requirements:
```bash
pip install -r requirements.txt
```
Dont forget to set an enviroment variable with this name for the file that will have the secret token to generate tokens for login:
```bash
ENV_FILE_LOCATION=path/to/.env
```
To run the project:
```bash
python server.py
```
To run the tests:
```bash
pytest tests.py -vv
```
- MongoDB is needed for API to work
