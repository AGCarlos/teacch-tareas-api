import bcrypt


def hash_password(new_user):
    return bcrypt.hashpw(new_user["password"].encode('utf8'), bcrypt.gensalt())


def check_password(user_password, hashed):
    return bcrypt.checkpw(user_password.encode('utf8'), hashed)
